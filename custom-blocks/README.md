# Custom blocks

You can put your custom blocks here.
The plocks should be in a sub-directory called the same as the variant of the statusbar (usually `normal`).
You can also put files in `~/.config/custom-i3-blocks/<VARIANT>`.
For more information see the [README.md in the parent directory](../README.md).

#!/usr/bin/bash

# Colors
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
RESET="\e[0m"

function log_error {
    echo -e "${RED}[!] $1${RESET}"
}

function log_success {
    echo -e "${GREEN}[+] $1${RESET}"
}

#######################################################
# Check arguments
#######################################################

# Check argument count
if [[ $# -ne 2 ]]; then
  echo "Usage: <PREPROCESSOR> <VARIANT>"
  echo "Where PREPROCESSOR is either 'none', 'default', 'vm', or a script/program that can rename, add or remove module files"
  echo "Where VARIANT is one of the folder names in the 'blocks' directory (likely 'normal', 'presentation', or 'small')"
  exit 1
fi

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
VARIANT="$2"
BUILD_DIR="${SCRIPT_DIR}/build/${VARIANT}"
CONFIG="${BUILD_DIR}/i3status-rust.toml"
USER_MODULE_DIR=~/.config/custom-i3-blocks/"${VARIANT}"

# Get preprocessor
PREPROCESSOR="$1"
if [[ "${PREPROCESSOR}" == "none" ]]; then
    # Do nothing
    true
elif [[ "${PREPROCESSOR}" == "default" || "${PREPROCESSOR}" == "vm" ]]; then
    # Handle some predefined choices
    PREPROCESSOR="${SCRIPT_DIR}/config/preprocessor_${PREPROCESSOR}.sh"
elif [[ -x "${PREPROCESSOR}" ]]; then
    # It is an executable file, everything is fine
    true
else
    log_error "Invalid preprocessor '${PREPROCESSOR}'. It should be 'none', 'default', 'vm', or a path to an executable file"
    exit 1
fi


#######################################################
# Copy over the module files into the build directory
#######################################################

# First clean up the output directory
if [[ -d "${BUILD_DIR}" ]]; then
    rm -r "${BUILD_DIR}"
fi
mkdir -p "${BUILD_DIR}"


# Actually copy over the default modules
cp "${SCRIPT_DIR}/blocks/${VARIANT}/"* "${BUILD_DIR}"

copy_custom_modules() {
    if [[ -d "$1" ]]; then
        CUSTOM_COUNT=`ls -1 "$1" | wc -l`
        if [[ CUSTOM_COUNT -ge 1 ]]; then
            echo "[*] Found ${CUSTOM_COUNT} custom module(s) in $1"
            cp "$1/"* "${BUILD_DIR}"
        fi
    fi
}

copy_custom_modules "${SCRIPT_DIR}/custom-blocks/${VARIANT}"
copy_custom_modules "${USER_MODULE_DIR}"


#######################################################
# Preprocessor step
#######################################################

# Move into the build directory
cd "${BUILD_DIR}"

# Running the preprocessor (or skipping them)
if [[ "${PREPROCESSOR}" != "none" ]]; then
    echo "[*] Running preprocessor"
    if ! "${PREPROCESSOR}"; then
        log_error "Preprocessor failed"
        exit 1
    fi
else
    echo "[*] Skipping preprocessor"
fi

#######################################################
# Generate the config
#######################################################

# This serves two purposes:
# (1) Create or clean up the file
# (2) Document from when the config is and that is ia automatically generated
echo -e "# ALL CHANGES MADE TO THIS FILE WILL BE LOST\n# Auto-generated on $(date) with $0\n" > "${CONFIG}"

process_file() {
    if [[ ! -f "$1" ]]; then
        echo "[-] File $1 does not exist"
        return 1
    fi

    # If the file extension is .toml then add it directly to the output file
    if [[ "$1" == *".toml" ]]; then
        # Do not process the config file we are currently building
        if [[ "$(basename "$1")" != "i3status-rust.toml" ]]; then
            echo "[*] Using raw config $1"
            echo "###################################################################" >> "${CONFIG}"
            echo "################### Copied from $1" >> "${CONFIG}"
            echo -e "###################################################################\n" >> "${CONFIG}"
            cat "$1" >> "${CONFIG}"
            echo -e "\n" >> "${CONFIG}"
        fi
    # If the file is a .link, then use the file(s) linked in their contents
    elif [[ "$1" == *.link ]]; then
            echo "###################################################################" >> "${CONFIG}"
        echo "################### Links from $1" >> "${CONFIG}"
        echo -e "###################################################################\n" >> "${CONFIG}"

        echo "[*] Using link file $1"
        while read -r line; do
            echo -e "################# $1 -> $line #################\n" >> "${CONFIG}"
            process_file "$line"
        done < "$1"
    # If a file is go file: compile it and execute it with the "config" argument
    elif [[ "$1" == *.go ]]; then
        echo "[*] Compiling and running executable $1"
        GO_COMPILED_FILE="$(basename "$1").compiled"
        go build -o "$GO_COMPILED_FILE" "$1"
        echo "###################################################################" >> "${CONFIG}"
        echo "################### Compiled from $1" and run >> "${CONFIG}"
        echo -e "###################################################################\n" >> "${CONFIG}"

        "./$GO_COMPILED_FILE" config >> "${CONFIG}"
        echo -e "\n\n" >> "${CONFIG}"
    # If a file is executable: execute it with the "config" argument
    elif [[ -x "$1" ]]; then
        echo "[*] Using executable $1"
        echo "###################################################################" >> "${CONFIG}"
        echo "################### Generated with $1" >> "${CONFIG}"
        echo -e "###################################################################\n" >> "${CONFIG}"

        "$1" config >> "${CONFIG}"
        echo -e "\n\n" >> "${CONFIG}"
    else
        echo "[-] No actions performed for $1"
    fi
}

# Iterate over all files in the directory
for module_file in ./*; do
    process_file "$module_file"
done

#######################################################
# Validate the config
#######################################################

# Make sure that the statusbar wil actually work
echo "[*] Validating generated configuration"
# @TODO: reintroduce --exit-on-error if they fix the error
VALIDATION_STDERR="$(timeout 0.2 i3status-rs "$CONFIG" 2>&1)"
STATUS=$?
if [[ ${STATUS} -eq 124 ]]; then
    # process killed by timeout -> seems to work without errors
    log_success "Validation passed"
else
    log_error "Validation failed with code ${STATUS}"
    log_error "Validation error message:"
    echo -e "${YELLOW}${VALIDATION_STDERR}${RESET}"
    exit 1
fi

#!/usr/bin/bash
# This is just a convenience program, that 
# 1) runs the build process
# 2) copies the resulting files to the dofault location
# 3) restarts i3

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Build all variants of the statusbar
for VARIANT in normal presentation small; do
    # Forward all agruments to the build script
    if ! "${SCRIPT_DIR}/build_config.sh" "$@" "$VARIANT"; then
        echo "Error: Building the statusbar variant $VARIANT failed"
        # For some reason I get an EOF error when this happens. Maybe because another i3status-rs instance was started?
        # But restarting i3 seems to fix it
        i3 restart >/dev/null
        exit 1
    fi
    # Copy the built config file
    cp "${SCRIPT_DIR}/build/$VARIANT/i3status-rust.toml" ~/.config/i3status-rust/$VARIANT.toml
done

rm ~/.config/i3status-rust/config.toml
ln -s ~/.config/i3status-rust/normal.toml ~/.config/i3status-rust/config.toml

if ! pkill -SIGUSR2 i3status-rs; then
    echo "Error: Restarting i3status-rs in-place failed"
    exit 1
fi
# # Restart i3
# if ! i3 restart >/dev/null; then
#     echo "Error: Restarting i3 failed"
#     exit 1
# fi


# My i3status-rust config

Config generator / custom blocks for [i3status-rust](https://github.com/greshake/i3status-rust).

Currently still a work in progress.

## Requirements

- [i3status-rust](https://github.com/greshake/i3status-rust) obviously

### Optional dependencies

- [Go compiler](https://go.dev/) for some of my custom blocks
- `xclip` for the clipboard block
- `safeeyes` for the [safeeyes](https://github.com/slgobinath/SafeEyes) block

## Usage

Just run `./build_config.sh none`, which will generate `./i3status-rust.toml`.
Then copy it to your desired location (or pass the configuration file to i3status) and reload `i3`:

```bash
cp ./i3status-rust.toml ~/.config/i3status-rust/config.toml && i3 restart
```

### Advanced usage

If you want to add custom blocks, add them to one of the custom block locations (see below, for block examples see `./blocks`).
If you don't want to remove / reorder some of the default blocks, you can write a custom script (called preprocessor) that performs the file removal / renaming (also see below, for examples see `./config/preprocessor_*.sh`).
Then call the script with your preprocessor:

```bash
./build_config.sh /path/to/preprocessor
```

## How it works

This is just a general overview and may not always be up to date.
But you can look up the details in the code, it should have some comments.

### Preparation

First the script collects all files from the following directories:

1. `./blocks/`
2. `./custom-blocks/`
3. `~/.config/custom-i3-blocks/`

Files from directories lower on the list will take priority in cases of name collisions.
The results will be stored in `./build`.

### Preprocessor

A custom script can be passed as a preprocessor.
It gets executed in the `./build` directory and can be used to add, rename, modify, and/or remove files.
If it returns an error code, this is considered a critical error and the whole program fails.

Basically I think of preprocessors like "profiles".
I have a different preprocessor for different systems (normal desktop, VM, etc).

### Config generation

The files are iterated over in alphabetic order (that is why the blocks are prefixed by numbers).
Depending on the type of each file. a block is generated from it:

- The contents of files ending in `.toml` are directly embedded into the config
- Executable files (execute/`x` permission set) will be executed with `config` as an argument. For example `00_block.sh` would be executed like this:
    ```bash
    ./00_block.sh config
    ```
    The output of the invocation is added to the config file.
- Go source files (`*.go`) are compiled using `go build <filename>`.
    The resulting executable is then treated as described above.

The full config file is then written to `./i3status-rust.toml`.

### Validation

After the config file is generated, it is tested by running the following command:
```bash
i3status-rs ./i3status-rust.toml --exit-on-error
```

If this does not generate an error within a short time (currently 200ms) it is considered to be valid.
This should find all syntax errors and some block errors, such as JSON block that does not produce valid JSON output.

#!/usr/bin/bash

case "$1" in
  config)
    # Find the first battery device
    DEVICE=$(ls -1 /sys/class/backlight/)
    if [[ -n "${DEVICE}" ]]; then
      cat << EOF
[[block]]
block = "backlight"
# Turn display off (without locking screen)
[[block.click]]
button = "right"
cmd = "sleep 0.5 && xset dpms force off"
EOF
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries to detect your battery name"
    exit 1
    ;;
esac

#!/usr/bin/bash

case "$1" in
  config)
    for DEVICE in /sys/class/power_supply/*; do
      DEVICE="$(basename "${DEVICE}")"
      cat << EOF
[[block]]
block = "battery"
device = "${DEVICE}"
missing_format = ""
full_format = " ${DEVICE} \$power "
interval = 10
format = " ${DEVICE} \$power "

EOF
    done
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries to detect your battery name"
    exit 1
    ;;
esac

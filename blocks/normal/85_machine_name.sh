#!/usr/bin/bash

DISPLAY_NAME_FILE="$HOME/.config/displayname"
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")

print_config() {
  cat << EOF
[[block]]
block = "custom"
command = "$1"
interval = "once"
hide_when_empty = true
EOF
}

case "$1" in
  config)
    if [[ -f "${DISPLAY_NAME_FILE}" ]]; then
      # Show the "custom displayname" if it exists (mostly useful for VMs)
      print_config "cat ${DISPLAY_NAME_FILE}"
    elif hostname &> /dev/null; then
      # Check if the hostname binary exists by calling it
      print_config "hostname"
    else
      # Fallback in case the hostname binary is not installed
      print_config "cat /etc/hostname"
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries show the hostname or a pretty displayname"
    echo "left-click -> action to perform when the block is clicked"
    exit 1
    ;;
esac

#!/usr/bin/bash

ICON="🗘"
BAD_THRESHOLD=14
WARN_THRESHOLD=7

EMPTY='{"text":""}'

case "$1" in
  show)
    DAYS_SINCE="$(last-update-date --relative)"
    if [[ $? -eq 0 ]]; then
      # We successfully got a value
      if [[ "${DAYS_SINCE}" -ge 1 ]]; then
        # The action was not performed today
        if [[ "${DAYS_SINCE}" -ge "${BAD_THRESHOLD}" ]]; then
          STATE="Critical"
        elif [[ "${DAYS_SINCE}" -ge "${WARN_THRESHOLD}" ]]; then
          STATE="Warning"
        else
          STATE="Idle"
        fi
        echo "{\"icon\":\"\", \"state\":\"${STATE}\", \"text\":\"${ICON} ${DAYS_SINCE}d\"}"
      else
        echo "${EMPTY}"
      fi
    else
      echo "${EMPTY}"
    fi
    ;;

  toggle)
    echo "Not implemented"
    ;;

  config)
    SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")
    cat << EOF
[[block]]
block = "custom"
command = "${SCRIPT_PATH} show"
json = true
interval = 3600
hide_when_empty = true
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} toggle"
EOF
    ;;

  *)
    echo "Usage: <show|config>"
    exit 1
    ;;
esac

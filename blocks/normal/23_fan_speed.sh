#!/usr/bin/bash

# @TODO: make colored block with configurable values?

case "$1" in
  config)
    JQ_QUERY=""
    if sensors -j | grep "dell_smm-isa-0000" &> /dev/null; then
      # Dell laptop CPU fan speed
      JQ_QUERY="'.[\\\"dell_smm-isa-0000\\\"].fan1.fan1_input'"
    fi
    if [[ -n "${JQ_QUERY}" ]]; then
      cat << EOF
[[block]]
block = "custom"
command = "echo \$(sensors -j 2>/dev/null | jq ${JQ_QUERY}) RPM"
interval = 1
EOF
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries (honestly not very well) to detect your CPU fan speed. Improvements are welcome"
    exit 1
    ;;
esac

#!/usr/bin/bash

case "$1" in
  config)
    # Find the first battery device
    DEVICE=$(echo /sys/class/power_supply/BAT* | cut -d " " -f 1)
    if [[ -n "${DEVICE}" ]]; then
      cat << EOF
[[block]]
block = "battery"
device = "$(basename "${DEVICE}")"
missing_format = " \$power "
full_format = ""
interval = 10
format = " \$icon \$percentage "
empty_format = " Battery at \$percentage, charge NOW! Battery at \$percentage, charge NOW! Battery at \$percentage, charge NOW! Battery at \$percentage, charge NOW! "
good = 95
info = 55
warning = 40
critical = 25
empty_threshold = 15
EOF
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries to detect your battery name"
    exit 1
    ;;
esac

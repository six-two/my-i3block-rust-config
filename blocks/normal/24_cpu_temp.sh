#!/usr/bin/bash

case "$1" in
  config)
    SENSOR=""
    JSON="$(sensors -j)"
    if echo "$JSON" | grep -q "k10temp-pci-"; then
      # Looks like a AMD CPU
      SENSOR="$(echo "$JSON" | grep -Eo 'k10temp-pci-[^"]+')"
    elif echo "$JSON" | grep -q "coretemp-isa-"; then
      # Looks like a Intel CPU. Since there sometimes are other sensors matching *-isa-*, we try to only use coretemp-isa=*
      SENSOR="$(echo "$JSON" | grep -Eo 'coretemp-isa-[^"]+')"
    elif echo "$JSON" | grep -q -- "-isa-"; then
      # Looks like a Intel CPU? Fallback
      SENSOR="*-isa-*"
    fi
    if [[ -n "${SENSOR}" ]]; then
      if echo "$SENSOR" | grep -q '\*'; then
        # I do not know what the sensor is called exactly, so I can not test how many temeratures it has
        SHOW_SPAN=1
      else
        TEMP_COUNT="$(echo "$JSON" | jq ".[\"$SENSOR\"]" | grep -E "temp.*input" | wc -l)"
        if [[ "$TEMP_COUNT" -eq 1 ]]; then
          # When only one temperature is measured, there is no sense in giving a span. It just takes up extra space and suggests higher accuracy than actually exists
          SHOW_SPAN=0
        else
          SHOW_SPAN=1
        fi
      fi

      if [[ "$SHOW_SPAN" -eq 1 ]]; then
        TEMP_FORMAT='$min - $max'
      else
        TEMP_FORMAT='$max'
      fi

      cat << EOF
[[block]]
block = "temperature"
interval = 3
format = " CPU ${TEMP_FORMAT} "
format_alt = " CPU \$average "
chip = "${SENSOR}"
good = 40
idle = 55
info = 70
warning = 85
EOF
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries (honestly not very well) to detect your CPU temperature sensor name. Improvements are welcome"
    exit 1
    ;;
esac

package main

// @TODO: Rewrite to work with continuous mode (i3status)
// @TODO: Whatch wehn the timer changes and then have a more accurate countdown

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

const I3STATUS_CONFIG = `[[block]]
block = "custom"
command = "%s status"
json = true
interval = 2
[[block.click]]
button = "left"
cmd = "%s toggle"
[[block.click]]
button = "right"
cmd = "safeeyes --take-break"
`

type block struct {
	Text  string `json:"text"`
	State string `json:"state,omitempty"`
	Icon  string `json:"icon,omitempty"`
}

// Create a block JSON string, and properly handle errors by converting them into a JSON block string
func CreateBlockJson(text string, state string, icon string) string {
	blockJson, blockError := internalCreateBlockJson(text, state, icon)
	if blockError == nil {
		// Expected case: Show the normal block
		return blockJson
	} else {
		errorBlockJson, errorBlockError := internalCreateBlockJson(blockError.Error(), "Critical", "")
		if errorBlockError == nil {
			// Error case: Show the error message
			return errorBlockJson
		} else {
			// Extreme fallback case: just show a generic error message
			return "{\"text\":\"Fatal error\", \"state\":\"Critical\"}"
		}
	}
}

func safeEyesRpcRequest(requestBody string) (response string, err error) {
	// The lazy approach is invoking "safeeyes --status", but it is absurdly slow ~200 milliseconds
	// out, err := exec.Command("safeeyes", "--status").Output()

	// The proper approach would be to use a XMLRPC library, but I have no experience with Go and libraries and it looks complicated

	// The current way is observing the request using wireshark, creating a curl invocation to replicate it and then do the invocation in Go and finally parse the results using regexes (if necessary).
	out, err := exec.Command("curl", "http://localhost:7200", "--data", requestBody).Output()
	// Other methods: enable_safeeyes,
	if err == nil {
		outString := string(out)
		if strings.Contains(outString, "<fault>") {
			// Our invocation seems to be wrong
			return outString, errors.New("Bad request")
		} else {
			return outString, nil
		}
	} else {
		return "", err
	}
}

func SafeEyesRpcGetStatus() (string, error) {
	const request = "<?xml version='1.0'?><methodCall><methodName>status</methodName><params></params></methodCall>"
	out, err := safeEyesRpcRequest(request)
	if err == nil {
		resultRegex := regexp.MustCompile("<string>([^<]+)</string>")
		match := resultRegex.FindStringSubmatch(out)
		if match != nil && len(match) == 2 {
			trimmed_value := strings.TrimSpace(match[1])
			return trimmed_value, nil
		} else {
			return "", errors.New("Unexpected response")
		}
	} else {
		return "", err
	}
}

func SafeEyesRpcDisable() (string, error) {
	const request = "<?xml version='1.0'?><methodCall><methodName>disable_safeeyes</methodName><params><value><nil/></value></params></methodCall>"
	return safeEyesRpcRequest(request)
}

func SafeEyesRpcEnable() (string, error) {
	const request = "<?xml version='1.0'?><methodCall><methodName>enable_safeeyes</methodName><params></params></methodCall>"
	return safeEyesRpcRequest(request)
}

func ToggleSafeEyesStatus() error {
	status, err := SafeEyesRpcGetStatus()
	if err != nil {
		// The XMLRPC service is probably not running, so we can not tell it to restart
		// So instead we start safeeyes manually with the --enable flag (in case this assumption is wrong)
		// We run it via wmc-msg (which delegates to i3-msg/swaymsg), so that it is executed in the background
		// @TODO: replace wmc-msg with reading program from a variable
		fmt.Println("Starting safeeyes")
		out, err2 := exec.Command("wmc-msg", "exec", "safeeyes --enable").CombinedOutput()
		if err2 != nil {
			fmt.Println("Output:", string(out))
		}
		return err2
	} else {
		if status == "Disabled until restart" {
			// disabled -> enable
			fmt.Println("Enabling safeeyes")
			_, err = SafeEyesRpcEnable()
		} else {
			// enabled -> disable
			fmt.Println("Disabling safeeyes")
			_, err = SafeEyesRpcDisable()
		}
	}
	return err
}

func GetTimeToNextBreak() (string, string) {
	status, err := SafeEyesRpcGetStatus()
	if err != nil {
		return "error", "Critical"
	}
	// Expected output:
	//   If disabled: Disabled until restart
	//   If enabled: Next break at 5:53 PM
	if status == "Disabled until restart" {
		return "off", "Warning"
	} else {
		// the character between the time and the AM/PM is strange and does get matched by \s (space) but sure looks like it.
		// It also screws up the date parsing, so we need to do this ugly workaround
		nextBreakRegex := regexp.MustCompile("Next break at ([0-1]?[0-9]:[0-5][0-9])[^AP]+([AP]M)")
		match := nextBreakRegex.FindStringSubmatch(status)
		if match != nil && len(match) == 3 {
			// concatinate time and AM/PM with a space
			breakTimeStr := fmt.Sprintf("%s %s", match[1], match[2])
			const layout = "3:04 PM"
			breakTime, _ := time.Parse(layout, breakTimeStr)

			nowTotalMins := datetimeToMinInDay(time.Now())
			breakTotalMin := datetimeToMinInDay(breakTime)
			timeToBreak := breakTotalMin - nowTotalMins

			if timeToBreak == -1 {
				return "active", ""
			}

			if timeToBreak < 0 {
				// This may be caused by a break being after midnight and the current time being before midnight
				// So we just add a day's worth of minutes
				timeToBreak += 24 * 60
			}

			// Decide which text to show dependent on the remaining time to break
			if timeToBreak == 0 {
				return "soon", "Good"
			} else if timeToBreak == 1 {
				return "1m", "Info"
			} else {
				return fmt.Sprintf("%dm", timeToBreak), ""
			}
		}
		// Try to somewhat gracefully handle the parsing error by showing the raw status
		// But color it as a warning, to show that it is not intended to look like this
		// This is also useful for debugging whenever the status format changes
		return status, "Warning"
	}
}

// Returns the time measured as the Nth minute of the day
func datetimeToMinInDay(theDatetime time.Time) int {
	hour, min, _ := theDatetime.Clock()
	return 60*hour + min
}

// An internal function that creates a block JSON string, but it may fail
func internalCreateBlockJson(text string, state string, icon string) (string, error) {
	if text == "" {
		return "", errors.New("Block has empty text")
	} else {
		myBlock := block{text, state, icon}
		bytes, jsonError := json.Marshal(myBlock)
		return string(bytes), jsonError
	}
}

func commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

func main() {
	if len(os.Args) == 2 && os.Args[1] == "status" {
		text, state := GetTimeToNextBreak()
		fmt.Println(CreateBlockJson("☕ "+text, state, ""))
	} else if len(os.Args) == 2 && os.Args[1] == "toggle" {
		err := ToggleSafeEyesStatus()
		if err != nil {
			fmt.Println("Error:", err.Error())
		}
	} else if len(os.Args) == 2 && os.Args[1] == "config" {
		// Only print something if safeeyes is installed
		if commandExists("safeeyes") {
			executable, err := os.Executable()
			if err == nil {
				validChars := regexp.MustCompile("^[a-zA-Z0-9-_./]+$")
				if validChars.MatchString(executable) {
					fmt.Printf(I3STATUS_CONFIG, executable, executable)
				} else {
					fmt.Fprintf(os.Stderr, "The executable path contains potentially problematic characters. Path: '%s'\n", executable)
					os.Exit(1)
				}
			} else {
				fmt.Fprintln(os.Stderr, "The program could not find the path to its own executable")
				os.Exit(1)
			}
		}
	} else {
		fmt.Println("Usage: [command]")
		fmt.Println("status  -> Output a i3status-rs compatible JSON block")
		fmt.Println("toggle  -> Enable / Disable safeeyes, depending on if it is currently running")
		fmt.Println("config  -> Print the block entry to paste in your i3status-rs.toml")
	}
}

#!/usr/bin/bash

SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")


function show_help {
  echo "Usage: <action>"
  echo
  echo "Possible actions:"
  echo " - left-click: Manually take a screenshot"
  echo " - right-click: Automatically take a screenshot (full screen, saved to ~/Desktop)"
  echo " - config: Print code to add to your i3status-rust.toml config file"
}

is_installed() {
    command -v "$1" &>/dev/null
}

wayland_select() {
  if is_installed grim; then
    if is_installed slurp; then
      # select area with slurp
      if is_installed swappy; then
        grim -g "$(slurp)" - | swappy -f -
      else
        grim -g "$(slurp)" "$HOME/Desktop/$(date +'%Y-%m-%d_%H-%M-%S').png"
      fi
    else # no slurp, screenshot full screen
      if is_installed swappy; then
        grim - | swappy -f -
      else
        grim "$HOME/Desktop/$(date +'%Y-%m-%d_%H-%M-%S').png"
      fi
    fi
  fi
}


case "$1" in
  # select area and decorate it
  left-click)
    if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
      wayland_select
    else # X11
      if flameshot --version; then
        flameshot gui
      fi
    fi
    ;;

  # screenshot whole screens
  right-click)
    if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
      if is_installed grim; then
        grim "$HOME/Desktop/$(date +'%Y-%m-%d_%H-%M-%S').png"
      fi
    else
      if is_installed flameshot; then
        flameshot full --path "$HOME/Desktop/"
      fi
    fi
    ;;

  # Run a subcommand in the background (to not block the statusbar)
  exec)
    shift
    if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
      swaymsg exec "$SCRIPT_PATH" "$@"
    else
      i3-msg exec "$SCRIPT_PATH" "$@"
    fi
    ;;

  config)
    SHOW=0
    if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
      if is_installed grim; then
        SHOW=1
      fi
    else
      if is_installed flameshot; then
        SHOW=1
      fi
    fi

    if [[ $SHOW -eq 1 ]]; then
      cat << EOF
[[block]]
block = "custom"
command = "echo '📷'"
interval = "once"
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} exec left-click"
[[block.click]]
button = "right"
cmd = "${SCRIPT_PATH} exec right-click"
EOF
    fi
    ;;

  *)
    show_help
    exit 1
    ;;
esac

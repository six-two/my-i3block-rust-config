#!/usr/bin/bash

ICON="🔨"
BAD_THRESHOLD=5
WARN_THRESHOLD=1

case "$1" in
  show)
    COUNT="$(todos --global --count)"
    if [[ $? -eq 0 && "${COUNT}" -ge 1 ]]; then
      # The action was not performed today
      if [[ "${COUNT}" -ge "${BAD_THRESHOLD}" ]]; then
        STATE="Critical"
      elif [[ "${COUNT}" -ge "${WARN_THRESHOLD}" ]]; then
        STATE="Warning"
      else
        STATE="Idle"
      fi
      echo "{\"icon\":\"\", \"state\":\"${STATE}\", \"text\":\"${ICON} ${COUNT}\"}"
    else
      echo '{"text":""}'
    fi
    ;;

  left-click)
    todos --global --notify
    ;;

  right-click)
    mousepad ~/.TODO
    ;;

  config)
    SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")
    cat << EOF
[[block]]
block = "custom"
command = "${SCRIPT_PATH} show"
json = true
interval = 60
hide_when_empty = true
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} left-click"
[[block.click]]
button = "right"
cmd = "${SCRIPT_PATH} right-click"
EOF
    ;;

  *)
    echo "Usage: <show|config|left-click|right-click>"
    exit 1
    ;;
esac

package main

// To perform some operations such as clipboard access and typing, some external programs are required:
// X11 dependencies: xclip xdotool
// Wayland dependencies: wl-clipboard (wl-copy, wl-paste), wtype

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"time"
	"unicode"
)

// This scring can be copied to create the Unicode problem
// 	<!DOCTYPE foo [ <!ENTITY

type block struct {
	Text  string `json:"text"`
	State string `json:"state,omitempty"`
	Icon  string `json:"icon,omitempty"`
}

const MAX_TEXT_LENGTH = 15
const EMPTY_BLOCK = `{"text":"📋"}`

// Create a block JSON string, and properly handle errors by converting them into a JSON block string
func CreateBlockJson(text string, state string, icon string) string {
	blockJson, blockError := internalCreateBlockJson(text, state, icon)
	if blockError == nil {
		// Expected case: Show the normal block
		return blockJson
	} else {
		errorBlockJson, errorBlockError := internalCreateBlockJson(blockError.Error(), "Critical", "")
		if errorBlockError == nil {
			// Error case: Show the error message
			return errorBlockJson
		} else {
			// Extreme fallback case: just show a generic error message
			return "{\"text\":\"Fatal error\", \"state\":\"Critical\"}"
		}
	}
}

func IsWayland() bool {
	return os.Getenv("XDG_SESSION_TYPE") == "wayland"
}

func TypeClipboardContents() {
	// Wait for the user to move the mouse to the target location / release the keys
	time.Sleep(1500 * time.Millisecond)
	var type_command string
	if IsWayland() {
		type_command = "wl-paste --no-newline | wtype -d 50 -"
	} else {
		type_command = "xclip -out -selection clipboard -rmlastnl | xdotool type --clearmodifiers --delay 50 --file -"
	}

	// Type the clipboard contents
	out, err := exec.Command("bash", "-c", type_command).Output()
	if err != nil {
		fmt.Println("Command failed:", err.Error())
		fmt.Println("Command output:", out)
	}
}

func CopyQrCodeContents() {
	out, err := exec.Command("copy-qr-code").Output()
	if err != nil {
		fmt.Println("Command failed:", err.Error())
		fmt.Println("Command output:", out)
	}
}

func GetClipboardContents() (string, error) {
	// @TODO: read clipboard in go or handle timoeout in go
	// Added timeout, since Virtualbox sometimes cuases issues with my clipboard which makes xclip freeze (which causes this block and thus the whole statusbar to freeze)
	if IsWayland() {
		out, err := exec.Command("timeout", "0.1", "wl-paste", "--no-newline").Output()
		return string(out), err
	} else {
		out, err := exec.Command("timeout", "0.1", "xclip", "-out", "-selection", "clipboard", "-rmlastnl").Output()
		return string(out), err
	}
}

func PrintStatusbarConfigBlock() {
	executable, err := os.Executable()
	if err == nil {
		validChars := regexp.MustCompile("^[a-zA-Z0-9-_./]+$")
		if validChars.MatchString(executable) {
			fmt.Printf(`[[block]]
block = "custom"
command = "%s status"
json = true
interval = 2
hide_when_empty = true
[[block.click]]
button = "left"
cmd = "%s on_click"
[[block.click]]
button = "right"
cmd = "%s right-click"
`, executable, executable, executable)
		} else {
			fmt.Fprintf(os.Stderr, "The executable path contains potentially problematic characters. Path: '%s'\n", executable)
			os.Exit(1)
		}
	} else {
		fmt.Fprintln(os.Stderr, "The program could not find the path to its own executable")
		os.Exit(1)
	}
}

// An internal function that creates a block JSON string, but it may fail
func internalCreateBlockJson(text string, state string, icon string) (string, error) {
	if text == "" {
		return "", errors.New("Block has empty text")
	} else {
		myBlock := block{text, state, icon}
		bytes, jsonError := json.Marshal(myBlock)
		// The statusbar seems to have problems with unicode characters, so we just replace them with a placeholder
		str := string(bytes)
		// Hm, that broke it again. Maybe the '<' causes the problem?
		// // Less than / greater than are used often, so we unescape them
		// str = strings.Replace(str, `\u003c`, "<", -1)
		unicode_regex := regexp.MustCompile(`\\u[0-9a-fA-F]{4}`)
		str = unicode_regex.ReplaceAllString(str, "[?]")
		return str, jsonError
	}
}

func GetClipboardJsonText() string {
	text, err := GetClipboardContents()
	if err == nil {
		clipboardLength := len(text)
		if clipboardLength == 0 {
			return EMPTY_BLOCK
		} else {
			// Show only one third of the clipboard contents (to protect potential passwords)
			cutoffLength := clipboardLength / 3
			if cutoffLength > MAX_TEXT_LENGTH {
				cutoffLength = MAX_TEXT_LENGTH
			}
			text = sanitizeString(text[:cutoffLength])
			text = fmt.Sprintf("📋 %s… (%d)", text, clipboardLength)

			// The stare (color) of the block
			state := "Good"
			if clipboardLength > 500 {
				state = "Critical"
			} else if clipboardLength > 100 {
				state = "Warning"
			}

			return CreateBlockJson(text, state, "")
		}
	} else {
		// This also happens when the window containing the copied text is closed, and we do not want to annoy the user with constantly red blocks
		return EMPTY_BLOCK
	}
}

// Replace all control characters with spaces (beacuse link breaks, etc can really screw stuff up)
func sanitizeString(problematic string) string {
	sanitized := make([]rune, 0, len(problematic))
	for _, r := range problematic {
		if unicode.IsControl(r) {
			if unicode.IsSpace(r) {
				// Replace all whitespace with a normal space
				sanitized = append(sanitized, 0x20)
			} else {
				// Replace all other characters with the REPLACEMENT CHARACTER (questionmark in a square)
				sanitized = append(sanitized, 0xFFFD)
			}
		} else {
			sanitized = append(sanitized, r)
		}
	}
	return string(sanitized)
}

func main() {
	if len(os.Args) == 2 && os.Args[1] == "status" {
		fmt.Println(GetClipboardJsonText())
	} else if len(os.Args) == 2 && os.Args[1] == "on_click" {
		TypeClipboardContents()
	} else if len(os.Args) == 2 && os.Args[1] == "right-click" {
		CopyQrCodeContents()
	} else if len(os.Args) == 2 && os.Args[1] == "config" {
		PrintStatusbarConfigBlock()
	} else {
		fmt.Println(`Usage: [command]
status      -> Output a i3status-rs compatible JSON block
on_click    -> Type the current clipboard contents after a short delay
right-click -> Copy QR code contents to clipboard
config      -> Print the block entry to paste in your i3status-rs.toml`)
	}
}

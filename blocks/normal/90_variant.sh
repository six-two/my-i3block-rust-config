#!/usr/bin/bash

DISPLAY_NAME_FILE="$HOME/.config/displayname"
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")
CONF_MINIMUM="$HOME/.config/i3status-rust/small.toml"
CONF_NORMAL="$HOME/.config/i3status-rust/normal.toml"
CONF_PRESENTATION="$HOME/.config/i3status-rust/presentation.toml" #@TODO: make it three or more modes: normal, short, presentation
CONF_PATH="$HOME/.config/i3status-rust/config.toml"


case "$1" in
  config)
    cat << EOF
[[block]]
block = "custom"
command = "${SCRIPT_PATH} name"
interval = "once"
hide_when_empty = true
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} left-click"
EOF
    ;;

  name)
    if [[ "$CONF_PATH" -ef "$CONF_NORMAL" ]]; then
      echo Normal
    elif [[ "$CONF_PATH" -ef "$CONF_MINIMUM" ]]; then
      echo Small
    elif [[ "$CONF_PATH" -ef "$CONF_PRESENTATION" ]]; then
      echo Presentation mode
    else
      # Should normally not happen
      echo "Normal?"
    fi
    ;;


  # select area and decorate it
  left-click)
    PARENT_NAME=$(basename "$SCRIPT_DIR")
    NEW_CONFIG=""
    if [[ "$CONF_PATH" -ef "$CONF_NORMAL" ]]; then
      NEW_CONFIG="$CONF_MINIMUM"
    elif [[ "$CONF_PATH" -ef "$CONF_MINIMUM" ]]; then
      NEW_CONFIG="$CONF_PRESENTATION"
    elif [[ "$CONF_PATH" -ef "$CONF_PRESENTATION" ]]; then
      NEW_CONFIG="$CONF_NORMAL"
    else
      # Should normally not happen
      NEW_CONFIG="$CONF_NORMAL"
    fi

    if [[ -n "$NEW_CONFIG" ]]; then
      # Switch to the new config file
      rm "$CONF_PATH"
      ln -s "$NEW_CONFIG" "$CONF_PATH"
      pkill -SIGUSR2 i3status-rs
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries show the hostname or a pretty displayname"
    echo "left-click -> action to perform when the block is clicked"
    exit 1
    ;;
esac

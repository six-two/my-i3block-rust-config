#!/usr/bin/bash

is_installed() {
    command -v "$1" &>/dev/null
}

case "$1" in
  config)
    # For NVIDIA GPUS use the nvidia block
    if is_installed nvidia-smi && [[ "$(nvidia-smi -L | grep "^GPU" | wc -l)" -ge 1 ]]; then
      cat << "EOF"
[[block]]
block = "nvidia_gpu"
interval = 3
format = " GPU $utilization $temperature "

EOF
    fi

    # For AMD GPUs use the normal sensor block
    SENSOR=""
    if sensors -j | grep "amdgpu-pci-" &> /dev/null; then
      # Looks like a AMD GPU
      SENSOR="amdgpu-pci-*"
    fi
    if [[ -n "${SENSOR}" ]]; then
      cat << EOF
[[block]]
block = "temperature"
interval = 3
format = " GPU \$max "
format_alt = " GPU \$average "
chip = "${SENSOR}"
good = 40
idle = 55
info = 70
warning = 85
EOF
    fi
    ;;

  *)
    echo "Usage: config"
    echo "config -> tries (honestly not very well) to detect your GPU temperature sensor name. Improvements are welcome"
    exit 1
    ;;
esac

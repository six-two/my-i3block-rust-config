#!/usr/bin/bash

DISABLE_FILE="/tmp/lock-on-inactivity.disabled"

#@TODO: make sure this script properly integrates (should be fine al song as the diable files are the same)

function show_help {
  echo "Usage: <action>"
  echo
  echo "Possible actions:"
  echo " - trigger <command>: Lock if enabled, otherwise do nothing"
  echo " - show: Show status as i3status-rust JSON block"
  echo " - enable / on: Enable screen locking"
  echo " - disable / off: Disable screen locking"
  echo " - toggle: Toggle between enabled and disabled"
  echo " - config: Print code to add to your i3status-rust.toml config file"
}


function action_enable {
  rm -f "${DISABLE_FILE}"
  status=$?
  return $status
}

function action_disable {
  touch "${DISABLE_FILE}"
  status=$?
  return $status
}


case "$1" in
  trigger)
    if [ ! -f "${DISABLE_FILE}" ]; then
      "$2"
    fi 
    ;;

  toggle)
    if [ -f "${DISABLE_FILE}" ]; then
      action_enable
    else
      action_disable
    fi
    ;;

  enable | on)
    action_enable
    ;;

  disable | off)
    action_disable
    ;;


  show)
    if [[ -f "${DISABLE_FILE}" ]]; then
      echo '{"icon":"", "state":"Critical", "text":"🔒"}'
    else
      echo '{"icon":"", "state":"Idle", "text":"🔒"}'
    fi
    ;;

  config)
    SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")
    cat << EOF
[[block]]
block = "custom"
command = "${SCRIPT_PATH} show"
json = true
interval = 2
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} toggle"
[[block.click]]
button = "right"
cmd = "i3-msg exec 'i3lock || slock'"
EOF
    ;;

  *)
    show_help
    exit 1
    ;;
esac

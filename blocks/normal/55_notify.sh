#!/usr/bin/bash

if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
  NOTIFY_DAEMON="mako"
else
  NOTIFY_DAEMON="${2:-xfce4-notifyd}"
fi

#@TODO: check args properly

action_show() {
  DND_ENABLED=0
  if [[ "$XDG_SESSION_TYPE" == wayland ]] && makoctl mode | grep --quiet do-not-disturb; then
    DND_ENABLED=1
  elif [[ "$XDG_SESSION_TYPE" == x11 ]] && ! pgrep -u "$(whoami)" "${NOTIFY_DAEMON}" &> /dev/null; then
    DND_ENABLED=1
  fi

  if [[ "$DND_ENABLED" -eq 0 ]]; then
    echo '{"icon":"", "state":"Idle", "text":"🔔"}'
  else
    echo '{"icon":"", "state":"Critical", "text":"🔔"}'
  fi
}

action_toggle_wayland() {
  # If necessary patch do-not-disturb mode that hides notifications to mako's config
  if [[ ! -f ~/.config/mako/config ]] || ! grep --quiet "mode=do-not-disturb" ~/.config/mako/config; then
    mkdir -p ~/.config/mako/
    cat << EOF > ~/.config/mako/config
[mode=do-not-disturb]
invisible=1
EOF
  fi

  # Just killing mako wont help, it will restart when the next notification is being sent
  if makoctl mode | grep --quiet do-not-disturb; then
    makoctl mode -r do-not-disturb
  else
    makoctl mode -a do-not-disturb
  fi
}

action_toggle_x11() {
  if pgrep -u "$(whoami)" "${NOTIFY_DAEMON}" &> /dev/null; then
    echo "Killing ${NOTIFY_DAEMON}"
    killall "${NOTIFY_DAEMON}"
  else
    echo "Starting ${NOTIFY_DAEMON}"
    # For me xfce4-notifyd is not in my default path, so we patch it here>
    export PATH="$PATH:/usr/lib/xfce4/notifyd/"
    # It seems i3 exec does not use the new path, so we explicitly resolve it first with which
    i3 exec "$(which ${NOTIFY_DAEMON})"
  fi
}

case "$1" in
  show)
    action_show
    ;;

  toggle)
    if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
      action_toggle_wayland
    else
      action_toggle_x11
    fi
    ;;

  config)
    SCRIPT_PATH=$(realpath "${BASH_SOURCE:-$0}")
    cat << EOF
[[block]]
block = "custom"
command = "${SCRIPT_PATH} show"
json = true
interval = 2
[[block.click]]
button = "left"
cmd = "${SCRIPT_PATH} toggle"
EOF
    ;;

  *)
    echo "Usage: <status|toggle> [notification-process-name]"
    echo "When no notification-process-name is give, 'xfce4-notifyd' will be used"
    exit 1
    ;;
esac

#!/usr/bin/bash

# It is the host, so I know what machine it is.
# The block is more meant for VMs, since they tend to look alike
rm *machine_name*

# This is meant for debugging/only for some special devices
rm *power.sh &>/dev/null

# Set the exit code to successful, no matter if the files were deleted
true

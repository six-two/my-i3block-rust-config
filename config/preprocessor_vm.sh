#!/usr/bin/bash

# The clipboard should not be removed, since shared clipboards are not always bidirectional / enabled

# Remove safeeyes, since this should generally be running on the host
rm *_lock* &>/dev/null

# These blocks are already shown on the host and would show the same values
rm *_fan_speed* &>/dev/null
rm *_temp* &>/dev/null
rm *_battery* &>/dev/null
rm *_backlight* &>/dev/null
# Time should not be removed, since time / timezone settings may be different

# This is meant for debugging/only for some special devices
rm *power.sh &>/dev/null

# Set the exit code to successful, no matter if the files were deleted
true
